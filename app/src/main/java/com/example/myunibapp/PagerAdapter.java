package com.example.myunibapp;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG_LOG = PagerAdapter.class.getName();
    int countTabs;

    public PagerAdapter(@NonNull FragmentManager fm, int countTabs) {
        super(fm);
        this.countTabs = countTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                Tab1Piano tab1 = new Tab1Piano();
                return tab1;
            case 1:
                Tab2Piano tab2 = new Tab2Piano();
                return tab2;
            case 2:
                Tab3Piano tab3 = new Tab3Piano();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return countTabs;
    }



}
