
package com.example.myunibapp;

        import androidx.appcompat.app.AppCompatActivity;
        import androidx.appcompat.widget.Toolbar;

        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Color;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.MenuItem;
        import android.widget.TextView;

        import com.example.myunibapp.DbManager;
        import com.github.mikephil.charting.animation.Easing;
        import com.github.mikephil.charting.charts.PieChart;
        import com.github.mikephil.charting.data.PieData;
        import com.github.mikephil.charting.data.PieDataSet;
        import com.github.mikephil.charting.data.PieEntry;
        import com.github.mikephil.charting.utils.ColorTemplate;

        import java.util.ArrayList;
        import java.util.Arrays;


public class StatisticheActivity extends AppCompatActivity {
    private static final String TAG_LOG = StatisticheActivity.class.getName();
    public String[] studente = new String[9];
    String matricola;
    float mediaPonderata;
    int votoLaurea;
    Bundle data;

    PieChart pieChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistiche);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Intent intentReceived = getIntent();
        data = intentReceived.getExtras();
        if(data != null) {
            matricola = data.getString("data");
        }
        Log.d(TAG_LOG, "Ecco matricola " + matricola);

        Context context = getApplicationContext();
        final DbManager db = new DbManager(context);

        studente = db.leggiStudente(matricola);
        Log.d(TAG_LOG, "Ecco studente " + Arrays.toString(studente));

        mediaPonderata = db.calcolaMediaPonderataStudente(matricola);
        votoLaurea = db.calcolaVotoLaurea(matricola, mediaPonderata);


        TextView media_aritmetica = (TextView) findViewById(R.id.media_aritmetica_valore);
        media_aritmetica.setText(studente[5]);

        TextView media_ponderata = (TextView) findViewById(R.id.media_ponderata_valore);
        media_ponderata.setText(String.valueOf(mediaPonderata));

        //TextView crediti_acquisiti = (TextView) findViewById(R.id.crediti_acquisiti);
        //crediti_acquisiti.setText(studente[7]);

        TextView voto_laurea = (TextView) findViewById(R.id.voto_laurea);
        voto_laurea.setText(String.valueOf(votoLaurea));

        //PieChart
        pieChart = (PieChart) findViewById(R.id.pieChart);

        pieChart.setUsePercentValues(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5 , 10 , 5 , 5);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setTransparentCircleRadius(61f);

        //Inserimento valori grafico a torta
        ArrayList<PieEntry> yValues = new ArrayList<>();

        yValues.add(new PieEntry(0 + db.calcoloCreditiAcquisiti(matricola) , "CFU Acquisiti"));
        yValues.add(new PieEntry(180 - db.calcoloCreditiAcquisiti(matricola) , "CFU Rimanenti"));
        //Se non va prova a mettere 180f

        pieChart.animateY(1000 , Easing.EasingOption.EaseInOutCubic);

        PieDataSet dataSet = new PieDataSet(yValues , "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.JOYFUL_COLORS);

        //Imposta testo valori percentuali
        PieData data = new PieData(dataSet);
        data.setValueTextSize(10f);
        data.setValueTextColor(Color.YELLOW);

        //Imposta colore testo percentuali PieChart
        pieChart.setEntryLabelColor(Color.BLACK);

        //Imposta testo centrale pieChart
        pieChart.setCenterText("CFU");
        pieChart.setCenterTextSize(14f);
        pieChart.setCenterTextColor(Color.BLUE);

        //Rimuove il testo dalle percentuali sul grafico (X Values)
        pieChart.setDrawEntryLabels(false);

        pieChart.setData(data);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
