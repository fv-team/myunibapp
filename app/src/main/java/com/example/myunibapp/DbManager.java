package com.example.myunibapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import static java.lang.Math.round;

public class DbManager {

    public static final short  CAMPI_DB_STUDENTE = 9;
    public static final short CAMPI_DB_DOCENTE = 5;
    public static final short CAMPI_DB_ESAME = 4;
    public static final short TOT_ESAMI = 21;

    private DBhelper dbhelper;
    public DbManager(Context ctx)
    {
        dbhelper = new DBhelper(ctx);
    }

    //Metodo che permette di inserire uno studente nel db
    //Restituisce l'ID dello studente se l'operazione va a buon fine, -1 altrimenti. Spesso esce -1 perchè il record è già presente nel db con la stessa matricola
    public long inserisciStudente(int matricola , String nome , String cognome , String email, String password , float media , float percEsamiSuperati , int creditiAcquisiti , String cdl)
    {
        long esito = 0;

        try {
            //Ci permette di ottenere il db in write mode
            SQLiteDatabase db = dbhelper.getWritableDatabase();

            ContentValues studente = new ContentValues();
            studente.put("Matricola" , matricola);
            studente.put("NomeStudente" , nome);
            studente.put("CognomeStudente" , cognome);
            studente.put("EmailStudente" , email);
            studente.put("PasswordStudente" , password);
            studente.put("MediaStudente" , media);
            studente.put("PercEsamiSuperati" , percEsamiSuperati);
            studente.put("CreditiAcquisiti" , creditiAcquisiti);
            studente.put("CorsoDiLaurea" , cdl);

            esito =  db.insert("Studenti", null , studente);

            db.close();

        } catch (SQLiteException sqlerror) {return esito;}

        return esito;
    }

    //Metodo che inserisce un docente nel db
    //Restituisce 1 se l'operazione va a buon fine, 0 altrimenti
    public short inserisciDocente(String id , String nome , String cognome , String email , String ricevimento)
    {
        try {
            //Ci permette di ottenere il db in write mode
            SQLiteDatabase db = dbhelper.getWritableDatabase();

            ContentValues docente = new ContentValues();
            docente.put("IdDocente" , id);
            docente.put("NomeDocente" , nome);
            docente.put("CognomeDocente" , cognome);
            docente.put("EmailDocente" , email);
            docente.put("RicevimentoDocente" , ricevimento);

            db.insert("Docenti" , null , docente);

            db.close();

        } catch (SQLiteException sqlerror) {return 0;}

        return 1;
    }

    //Metodo che inserisce un esame nel db
    //Restituisce 1 se l'operazione va a buon fine, 0 altrimenti
    public short inserisciEsame(String idEsame , String nome , String cfu , String annoEsame ,  String idDocente)
    {
        try {
            //Ci permette di ottenere il db in write mode
            SQLiteDatabase db = dbhelper.getWritableDatabase();

            ContentValues esame = new ContentValues();
            esame.put("IdEsame" , idEsame);
            esame.put("NomeEsame" , nome);
            esame.put("PesoInCrediti" , cfu);
            esame.put("AnnoEsame" , annoEsame);
            esame.put("IdDocente" , idDocente);

            db.insert("Esami" , null , esame);

            db.close();

        } catch (SQLiteException sqlerror) {return 0;}

        return 1;
    }

    //Metodo che inserisce un esame superato nel db
    //Restituisce 1 se l'operazione va a buon fine, 0 altrimenti
    public short inserisciEsameSuperato(String idRecord , String idEsame , String matricola , String voto , String data)
    {
        try {
            //Ci permette di ottenere il db in write mode
            SQLiteDatabase db = dbhelper.getWritableDatabase();

            ContentValues esameSuperato = new ContentValues();
            esameSuperato.put("Id" , idRecord);
            esameSuperato.put("IdEsame" , idEsame);
            esameSuperato.put("Matricola" , matricola);
            esameSuperato.put("VotoEsame" , voto);
            esameSuperato.put("DataEsame" , data);

            db.insert("EsamiSuperati" , null , esameSuperato);

            db.close();

        } catch (SQLiteException sqlerror) {return 0;}

        return 1;
    }

    //Metodo che restituisce uno studente data la matricola
    //Restituisce l'array dello studente dove ogni indice è un campo della tabella studente se l'operazione va a buon fine, altrimenti l'array restituito è pieno di zeri
    public String[] leggiStudente(String matricolaInput)
    {
        String[] studente = new String[CAMPI_DB_STUDENTE];
        for(short i = 0; i < studente.length; i++)
            studente[i] = "0";

        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM Studenti WHERE Matricola = ?" , new String[]{matricolaInput});
            if (c.moveToFirst())
                {
                    do {
                           // Passing values
                           // String matricola = c.getString(c.getColumnIndex("Matricola"));
                            String matricola = c.getString(0);
                            String nome = c.getString(1);
                            String cognome = c.getString(2);
                            String email = c.getString(3);
                            String password = c.getString(4);
                            String media = c.getString(5);
                            String percentualeEsamiSuperati = c.getString(6);
                            String creditiAcquisiti = c.getString(7);
                            String corsoDiLaurea = c.getString(8);

                            // Do something Here with values
                            studente[0] = matricola;
                            studente[1] = nome;
                            studente[2] = cognome;
                            studente[3] = email;
                            studente[4] = password;
                            studente[5] = media;
                            studente[6] = percentualeEsamiSuperati;
                            studente[7] = creditiAcquisiti;
                            studente[8] = corsoDiLaurea;

                       } while(c.moveToNext());
                }

            c.close();
            db.close();

            } catch (SQLiteException sqlerror) {return studente;}

            return studente;
    }

    //Metodo che restituisce un docente dato l'id
    //Restituisce l'array del docente se l'operazione va a buon fine, altrimenti l'array restituito è pieno di zeri
    public String[] leggiDocente(String idDocenteInput)
    {
        String[] docente = new String[CAMPI_DB_DOCENTE];
        for(short i = 0; i < docente.length; i++)
            docente[i] = "0";

        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM Docenti WHERE IdDocente = ?" , new String[]{idDocenteInput});
            if (c.moveToFirst())
            {
                do {
                    // Passing values
                    String id = c.getString(0);
                    String nome = c.getString(1);
                    String cognome = c.getString(2);
                    String email = c.getString(3);
                    String ricevimento = c.getString(4);

                    // Do something Here with values
                    docente[0] = id;
                    docente[1] = nome;
                    docente[2] = cognome;
                    docente[3] = email;
                    docente[4] = ricevimento;

                } while(c.moveToNext());
            }

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return docente;}

        return docente;
    }

    //Metodo che restituisce un esame dato l'id
    //Restituisce l'array dell'esame se l'operazione va a buon fine, altrimenti l'array restituito è pieno di zeri
    public String[] leggiEsame(String idEsameInput)
    {
        String[] esame = new String[CAMPI_DB_ESAME];
        for(short i = 0; i < esame.length; i++)
            esame[i] = "0";

        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM Esami WHERE IdEsame = ?" , new String[]{idEsameInput});
            if (c.moveToFirst())
            {
                do {
                    // Passing values
                    String idEsame = c.getString(0);
                    String nome = c.getString(1);
                    String cfu = c.getString(2);
                    String idDocente = c.getString(3);

                    // Do something Here with values
                    esame[0] = idEsame;
                    esame[1] = nome;
                    esame[2] = cfu;
                    esame[3] = idDocente;

                } while(c.moveToNext());
            }

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return esame;}

        return esame;
    }

    //Metodo che restituisce il numero di esami superati data la matricola
    //Utile per creare un array di dimensione pari al numero di esami superati
    //Non sapendo a priori il numero di esami superati di uno studente ci viene in aiuto questo metodo
    public short contaEsamiSuperati(String matricolaInput)
    {
        short totEsamiSuperati = 0;

        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT COUNT(*) AS TotEsamiSuperati " +
                    "FROM EsamiSuperati " +
                    "WHERE Matricola = ?" , new String[]{matricolaInput});

            if (c.moveToFirst())
            {

                do {
                    // Passing values
                    totEsamiSuperati = c.getShort(0);

                    // Do something Here with values

                } while(c.moveToNext());
            }

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return totEsamiSuperati;}

        return totEsamiSuperati;
    }


    //Metodo che restituisce gli esami superati data la matricola
    //Restituisce l'array degli esami superati se l'operazione va a buon fine, altrimenti l'array restituito è pieno di zeri
    //Restituisce in ordine NomeEsame , Voto , Data , Cfu
    public String[][] leggiEsamiSuperati(String matricolaInput) {

        short totEsamiSuperati = contaEsamiSuperati(matricolaInput);

        String[][] esamiSuperati = new String[totEsamiSuperati][4];
        for (short i = 0; i < esamiSuperati.length; i++) {
            esamiSuperati[i][0] = "0";
            esamiSuperati[i][1] = "0";
            esamiSuperati[i][2] = "0";
            esamiSuperati[i][3] = "0";
            }

        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT E.NomeEsame , S.VotoEsame , S.DataEsame , E.PesoInCrediti " +
                    "FROM Esami E INNER JOIN EsamiSuperati S ON E.IdEsame = S.IdEsame " +
                    "WHERE S.Matricola = ?" , new String[]{matricolaInput});
            if (c.moveToFirst())
            {
                short i = 0;

                do {
                    // Passing values
                    String nomeEsame = c.getString(0);
                    String votoEsame = c.getString(1);
                    String dataEsame = c.getString(2);
                    String cfuEsame = c.getString(3);

                    // Do something Here with values
                    esamiSuperati[i][0] = nomeEsame;
                    esamiSuperati[i][1] = votoEsame;
                    esamiSuperati[i][2] = dataEsame;
                    esamiSuperati[i][3] = cfuEsame;

                    i++;

                } while(c.moveToNext());
            }

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return esamiSuperati;}

        return esamiSuperati;
    }

    //Metodo che restituisce il numero di esami non superati data la matricola
    //Utile per creare un array di dimensione pari al numero di esami non superati
    //Non sapendo a priori il numero di esami non superati di uno studente ci viene in aiuto questo metodo
    public short contaEsamiNonSuperati(String matricolaInput)
    {
        short totEsamiNonSuperati = 0;

        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT COUNT(*) AS TotEsamiNonSuperati " +
                    "FROM Esami E INNER JOIN EsamiSuperati S ON E.IdEsame = S.IdEsame " +
                    "WHERE E.IdESame NOT IN (SELECT IdEsame FROM EsamiSuperati WHERE Matricola = ?)" , new String[]{matricolaInput});

            if (c.moveToFirst())
            {

                do {
                    // Passing values
                    totEsamiNonSuperati = c.getShort(0);

                    // Do something Here with values

                } while(c.moveToNext());
            }

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return totEsamiNonSuperati;}

        return totEsamiNonSuperati;
    }

    //Metodo che restituisce gli esami superati data la matricola
    //Restituisce l'array degli esami superati se l'operazione va a buon fine, altrimenti l'array restituito è pieno di zeri
    //Restituisce in ordine idEsame , nomeEsame, Cfu
    public String[][] leggiEsamiNonSuperati(String matricolaInput) {

        short totEsamiNonSuperati = contaEsamiNonSuperati(matricolaInput);

        String[][] esamiNonSuperati = new String[totEsamiNonSuperati][3];
        for (short i = 0; i < esamiNonSuperati.length; i++) {
            esamiNonSuperati[i][0] = "0";
            esamiNonSuperati[i][1] = "0";
            esamiNonSuperati[i][2] = "0";
        }

        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT E.IdEsame , E.NomeEsame , E.PesoInCrediti " +
                    "FROM Esami E INNER JOIN EsamiSuperati S ON E.IdEsame = S.IdEsame " +
                    "WHERE E.IdESame NOT IN (SELECT IdEsame FROM EsamiSuperati WHERE Matricola = ?)" , new String[]{matricolaInput});
            if (c.moveToFirst())
            {
                short i = 0;

                do {
                    // Passing values
                    String idEsame = c.getString(0);
                    String nomeEsame = c.getString(1);
                    String cfuEsame = c.getString(2);

                    // Do something Here with values
                    esamiNonSuperati[i][0] = idEsame;
                    esamiNonSuperati[i][1] = nomeEsame;
                    esamiNonSuperati[i][2] = cfuEsame;

                    i++;

                } while(c.moveToNext());
            }

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return esamiNonSuperati;}

        return esamiNonSuperati;
    }

    //Restituisce un'array dove ogni indice è un esame del piano di studi se l'operazione va a buon fine,
    // altrimenti l'array restituito è pieno di zeri
    public String[][] leggiPianoDiStudi(String annoPianoDiStudiInput)
    {
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT NomeEsame, PesoInCrediti, IdDocente FROM ESAMI WHERE AnnoEsame = ?" , new String[]{annoPianoDiStudiInput});
        String[][] pianoDiStudi = new String[c.getCount()][3];
        try {
            //Ci permette di ottenere il db in read mode

            if (c.moveToFirst())
            {
                short i = 0;

                do {
                    // Passing values
                    String nomeEsame = c.getString(0);
                    String cfu = c.getString(1);
                    String idDocente = c.getString(2);

                    // Do something Here with values
                    pianoDiStudi[i][0] = nomeEsame;
                    pianoDiStudi[i][1] = cfu;
                    pianoDiStudi[i][2] = idDocente;
                    i++;

                } while(c.moveToNext());
            }

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return pianoDiStudi;}

        return pianoDiStudi;
    }

    //Calcola la media dello studente arrotondata per eccesso se la seconda cifra decimale è >=5 , per difetto altrimenti
    public float calcolaMediaStudente(String matricolaInput)
    {
        float media = 0;
        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT ROUND(AVG(VotoEsame) , 2) FROM EsamiSuperati WHERE Matricola = ? " , new String[]{matricolaInput});
            if (c.moveToFirst())
            {
                do {
                    // Passing values
                    media = c.getFloat(0);

                    // Do something Here with values

                } while(c.moveToNext());
            }

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return media;}

        return media;
    }

    //Restituisce la percentuale degli esami superati della matricola data in input
    public float calcoloPercEsamiSuperati(String matricolaInput)
    {
        //(esami superati / esami totali)*100
        //Gli esami totali sono 21
        int esamiSuperati = 0;
        float percentualeEsamiSuperati = 0;
        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT COUNT(*) AS TotEsamiSuperati FROM EsamiSuperati WHERE Matricola = ? " , new String[]{matricolaInput});
            if (c.moveToFirst())
            {
                do {
                    // Passing values
                    esamiSuperati = c.getInt(0);

                    // Do something Here with values
                    //Arrotondiamo per eccesso se la cifra decimale è >=5, per difetto altrimenti;
                    percentualeEsamiSuperati = Math.round((esamiSuperati/21.0)*100.0);

                } while(c.moveToNext());
            }

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return percentualeEsamiSuperati;}

        return percentualeEsamiSuperati;

    }

    //Restituisce il totale dei crediti degli esami superati
    public int calcoloCreditiAcquisiti(String matricolaInput)
    {
        //Gli esami totali sono 21
        int creditiAcquisiti = 0;
        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT SUM(PesoInCrediti) AS CreditiAcquisiti " +
                    "FROM Esami E INNER JOIN EsamiSuperati S  ON E.IdEsame = S.IdEsame " +
                    "WHERE S.Matricola = ?" , new String[]{matricolaInput});
            if (c.moveToFirst())
            {
                do {
                    // Passing values
                    creditiAcquisiti = c.getInt(0);

                    // Do something Here with values

                } while(c.moveToNext());
            }

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return creditiAcquisiti;}

        return creditiAcquisiti;

    }

    //Calcola la media ponderata dello studente, arrotondata per eccesso se la seconda cifra decimale è >=5 , per difetto altrimenti
    public float calcolaMediaPonderataStudente(String matricolaInput)
    {
        //Bisogna moltiplicare il voto di ogni esame per il numero di crediti corrispondente
        //sommare tutti i risultati e dividere per il numero totale di crediti ottenuti
        //Matrice che contiene al primo indice l'id dell'esame, al secondo il voto, al terzo i cfu
        int[][] recordEsame = new int[21][3]; //21 righe = tot esami; 3 colonne = id, voto esame, cfu esame
            for(short i = 0; i < recordEsame.length; i++) {
                recordEsame[i][0] = 0;
                recordEsame[i][1] = 0;
                recordEsame[i][2] = 0;
            }

        float mediaPonderata = 0;
        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            //Otteniamo i voti di tutti gli esami effettuati e li mettiamo in un array di interi
            Cursor c = db.rawQuery("SELECT E.IdEsame , E.VotoEsame , S.PesoInCrediti " +
            "FROM EsamiSuperati E INNER JOIN Esami S ON E.IdEsame = S.IdEsame WHERE E.Matricola = ?" , new String[]{matricolaInput});
            if (c.moveToFirst())
            {
                short i = 0;

                do {
                    // Passing values
                   int idEsame = c.getInt(0);
                   int votoEsame = c.getInt(1);
                   int cfuEsame = c.getInt(2);

                    // Do something Here with values
                    recordEsame[i][0] = idEsame;
                    recordEsame[i][1] = votoEsame;
                    recordEsame[i][2] = cfuEsame;

                    i++;


                } while(c.moveToNext());
            }

            //Calcolo media ponderata
            int[] moltiplicazioneVotoCrediti = new int[21];
            int sommaVotoCrediti = 0;
            int sommaCfuOttenuti = 0;

            for(short i = 0; i < moltiplicazioneVotoCrediti.length; i++)
                moltiplicazioneVotoCrediti[i] = 0;

            for(short i = 0; i < recordEsame.length; i++)
            {
                moltiplicazioneVotoCrediti[i] = recordEsame[i][1]*recordEsame[i][2];
                sommaVotoCrediti += moltiplicazioneVotoCrediti[i];
                sommaCfuOttenuti += recordEsame[i][2];
            }

            mediaPonderata = Math.round(sommaVotoCrediti/sommaCfuOttenuti);

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return mediaPonderata;}

        return mediaPonderata;
    }

    //Calcola la media ponderata dello studente, arrotondata per eccesso se la seconda cifra decimale è >=5 , per difetto altrimenti
    public float calcolaStimaMediaPonderataStudente(String matricolaInput, int VotoEsameStimato, int CfuEsameStimato)
    {
        //Bisogna moltiplicare il voto di ogni esame per il numero di crediti corrispondente
        //sommare tutti i risultati e dividere per il numero totale di crediti ottenuti
        //Matrice che contiene al primo indice l'id dell'esame, al secondo il voto, al terzo i cfu
        int[][] recordEsame = new int[21][3]; //21 righe = tot esami; 3 colonne = id, voto esame, cfu esame
        for(short i = 0; i < recordEsame.length; i++) {
            recordEsame[i][0] = 0;
            recordEsame[i][1] = 0;
            recordEsame[i][2] = 0;
        }

        float mediaPonderata = 0;
        try {
            //Ci permette di ottenere il db in read mode
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            //Otteniamo i voti di tutti gli esami effettuati e li mettiamo in un array di interi
            Cursor c = db.rawQuery("SELECT E.IdEsame , E.VotoEsame , S.PesoInCrediti " +
                    "FROM EsamiSuperati E INNER JOIN Esami S ON E.IdEsame = S.IdEsame WHERE E.Matricola = ?" , new String[]{matricolaInput});
            if (c.moveToFirst())
            {
                short i = 0;

                do {
                    // Passing values
                    int idEsame = c.getInt(0);
                    int votoEsame = c.getInt(1);
                    int cfuEsame = c.getInt(2);

                    // Do something Here with values
                    recordEsame[i][0] = idEsame;
                    recordEsame[i][1] = votoEsame;
                    recordEsame[i][2] = cfuEsame;

                    i++;


                } while(c.moveToNext());
            }

            //Calcolo media ponderata
            int[] moltiplicazioneVotoCrediti = new int[21];
            int sommaVotoCrediti = 0;
            int sommaCfuOttenuti = 0;

            for(short i = 0; i < moltiplicazioneVotoCrediti.length; i++)
                moltiplicazioneVotoCrediti[i] = 0;

            for(short i = 0; i < recordEsame.length; i++)
            {
                moltiplicazioneVotoCrediti[i] = recordEsame[i][1]*recordEsame[i][2];
                sommaVotoCrediti += moltiplicazioneVotoCrediti[i];
                sommaCfuOttenuti += recordEsame[i][2];
            }

            mediaPonderata = Math.round((sommaVotoCrediti+VotoEsameStimato)/(sommaCfuOttenuti+CfuEsameStimato));

            c.close();
            db.close();

        } catch (SQLiteException sqlerror) {return mediaPonderata;}

        return mediaPonderata;
    }

    //Calcola il voto di laurea dello studente
    //arrotondata per eccesso se la seconda cifra decimale è >=5 , per difetto altrimenti
    public int calcolaVotoLaurea(String matricolaInput , float mediaPonderataInput)
    {
        int votoLaurea = 0;

        //Prendere la media ponderata, moltiplicarla per 110 e dividerla per 30
        votoLaurea = Math.round((mediaPonderataInput*110)/30);

        return votoLaurea;
    }

}
