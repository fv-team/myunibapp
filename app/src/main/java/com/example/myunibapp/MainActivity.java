package com.example.myunibapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageView;

import com.example.myunibapp.fragments.ProfileFragment;
import com.example.myunibapp.fragments.InformationFragment;
import com.example.myunibapp.ui.home.HomeFragment;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private static final String TAG_LOG = MainActivity.class.getName();
    ChipNavigationBar bottomNav;
    FragmentManager fragmentManager;
    ImageView logo;
    public String[] studente = new String[9];
    Bundle data;
    String matricolaDigitata;
    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intentReceived = getIntent();
         data = intentReceived.getExtras();
        if(data != null) {
            matricolaDigitata = data.getString("data");
        }

        bottomNav = findViewById(R.id.bottom_nav);

        if (savedInstanceState == null) {
            bottomNav.setItemSelected(R.id.nav_home, true);
            fragmentManager = getSupportFragmentManager();
            HomeFragment homeFragment = new HomeFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, homeFragment)
                    .commit();

        }

        bottomNav.setOnItemSelectedListener(new ChipNavigationBar.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int id) {
                Fragment fragment = null;
                switch (id) {
                    case R.id.home:
                        fragment = new HomeFragment();
                        break;
                    case R.id.profile:
                        fragment = new ProfileFragment();
                        break;
                    case R.id.information:
                        fragment = new InformationFragment();
                        break;
             }

                if (fragment != null) {
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, fragment)
                            .commit();
                }
                else {
                    Log.d(TAG_LOG, "Errore");
                }
            }
        });


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Context context = getApplicationContext();
        final DbManager db = new DbManager(context);

        studente = db.leggiStudente(matricolaDigitata);


    }

    public Bundle getMyData() {
        Bundle hm = new Bundle();
        hm.putString("val1",matricolaDigitata);
        Log.d(TAG_LOG, "Ecco quella della main Activity " + hm);
        return hm;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity, menu);
        return true;
    }

  



}
