package com.example.myunibapp;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myunibapp.recyclerview.ExamAdapter;

import java.util.Arrays;
import java.util.List;


public class Tab3Piano extends Fragment {

    private static final String TAG_LOG = Tab1Piano.class.getName();;
    private RecyclerView rvExams;
    private ExamAdapter examAdapter;
    private String[][] piano_di_studi;
    View v;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Context context = getActivity().getApplicationContext();
        final DbManager db = new DbManager(context);
        super.onCreate(savedInstanceState);

        piano_di_studi = db.leggiPianoDiStudi("3");
        Log.d(TAG_LOG, "Ecco matricola " + Arrays.toString(piano_di_studi));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_tab1, container, false);
        initViews();
        setupExamAdapter();
        return v;
    }

    private void setupExamAdapter() {
        examAdapter = new ExamAdapter(getContext(), piano_di_studi);
        rvExams.setAdapter(examAdapter);
        rvExams.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initViews() {
        rvExams = (RecyclerView) v.findViewById(R.id.rv_exam);
        rvExams.setLayoutManager(new LinearLayoutManager(getActivity()));
        /*rvExams.setHasFixedSize(true);*/
    }
}
