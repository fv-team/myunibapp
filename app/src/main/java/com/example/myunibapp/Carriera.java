package com.example.myunibapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;

import java.util.Arrays;

public class Carriera extends AppCompatActivity implements TabLayout.OnTabSelectedListener {
    private static final String TAG_LOG = StatisticheActivity.class.getName();
    public String[] studente = new String[9];
    float mediaPonderataStimata;
    String matricola;
    Bundle data;
    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carriera);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Intent intentReceived = getIntent();
        data = intentReceived.getExtras();
        if(data != null) {
            matricola = data.getString("data");
        }

        Log.d(TAG_LOG, "Ecco matricola da carriera" + matricola);

        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Superati"));
        tabLayout.addTab(tabLayout.newTab().setText("Da sostenere"));

        viewPager = findViewById(R.id.viewpager);

        PagerAdapterCareer pagerAdapterCareer = new PagerAdapterCareer(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(pagerAdapterCareer);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(this);


        Context context = getApplicationContext();
        final DbManager db = new DbManager(context);

        studente = db.leggiStudente(matricola);


        mediaPonderataStimata = db.calcolaStimaMediaPonderataStudente(matricola, 25, 12);
        Log.d(TAG_LOG, "Ecco studente " + mediaPonderataStimata);

    }

    public Bundle getMyData() {
        Bundle hm = new Bundle();
        hm.putString("val1",matricola);
        Log.d(TAG_LOG, "Ecco quella " + hm);
        return hm;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        Log.d(TAG_LOG, "Tab select" );
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}