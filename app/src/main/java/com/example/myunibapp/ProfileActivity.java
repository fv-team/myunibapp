package com.example.myunibapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Arrays;

public class ProfileActivity extends AppCompatActivity {
    public static final short CAMPI_DB_STUDENTE = 9;
    public String[] studente = new String[CAMPI_DB_STUDENTE];
    private static final String TAG_LOG = ProfileActivity.class.getName();
    String matricola;
    Bundle data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);

        Intent intentReceived = getIntent();
        data = intentReceived.getExtras();
        if(data != null) {
            matricola = data.getString("data");
        }
        Log.d(TAG_LOG, "Ecco matricola del profilo" + matricola);

        Context context = getApplicationContext();
        final DbManager db = new DbManager(context);

        studente = db.leggiStudente(matricola);
        Log.d(TAG_LOG, "Ecco studente dal profilo" + Arrays.toString(studente));

        TextView nome = (TextView) findViewById(R.id.name);
        nome.setText(studente[1]);

        TextView cognome = (TextView) findViewById(R.id.surname);
        cognome.setText(studente[2]);

        TextView email = (TextView) findViewById(R.id.email);
        email.setText(studente[3]);

        TextView corso = (TextView) findViewById(R.id.corso);
        corso.setText(studente[8]);
    }
}
