package com.example.myunibapp.recyclerview;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;
import com.example.myunibapp.DbManager;
import com.example.myunibapp.R;

import static androidx.core.content.ContextCompat.startActivity;


public class ExamAdapter extends RecyclerView.Adapter<ExamAdapter.examviewholder> {
    private static final String TAG_LOG = ExamAdapter.class.getName();
    public static final short CAMPI_DB_DOCENTE = 5;
    Context mContext;
    String[][] mData;
    Dialog myDialog;
    public String[] docente = new String[CAMPI_DB_DOCENTE];
    int ciao;


    public ExamAdapter(Context mContext, String[][] mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    //Metodo per inviare una mail al docente. Il destinatario è già precompilato
    //in base al docente che si vuole contattare dopo aver premuto "Contatta" nell'apposita finestra
    //di dialogo
    public void SendEmail(String[] address) {
        Context context = this.mContext;
        Bundle bundle= new Bundle();

        Intent email = new Intent(Intent.ACTION_SENDTO);
        //email.setType("*/*");
        //email.setType("message/rfc822");
        email.setData(Uri.parse("mailto:")); // Solo app email dovrebbero risolvere questo
        email.putExtra(Intent.EXTRA_EMAIL, address);
        //email.putExtra(Intent.EXTRA_SUBJECT, subject);
        startActivity(context , email , bundle);
        }

    @NonNull
    @Override
    public examviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_exam, parent, false);
        final examviewholder vHolder = new examviewholder(view);

        //Dialog
        myDialog = new Dialog(mContext);
        myDialog.setContentView(R.layout.dialog_professor);
        final Context context = mContext.getApplicationContext();
        final DbManager db = new DbManager(mContext);

        vHolder.professorIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView dialog_name_professor = (TextView) myDialog.findViewById(R.id.dialog_name_id);
                TextView dialog_surname_professor = (TextView) myDialog.findViewById(R.id.dialog_surname_id);
                TextView dialog_email_professor = (TextView) myDialog.findViewById(R.id.dialog_email_id);
                TextView dialog_time_professor = (TextView) myDialog.findViewById(R.id.dialog_time_id);
                ciao = Integer.valueOf(vHolder.getAdapterPosition());
                Log.d(TAG_LOG, "Pollo " + db.leggiDocente(mData[ciao][2]));
                docente = db.leggiDocente(mData[ciao][2]);
                dialog_name_professor.setText(docente[1]);
                dialog_surname_professor.setText(docente[2]);
                dialog_email_professor.setText(docente[3]);
                dialog_time_professor.setText(docente[4]);
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.show();

                ((ImageButton) myDialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                ((AppCompatButton) myDialog.findViewById(R.id.bt_follow)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, R.string.toast_attendi, Toast.LENGTH_SHORT).show();
                        SendEmail(new String[]{docente[3]});
                    }
                });
            }
        });
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull examviewholder holder, int position) {
            holder.title.setText(mData[position][0]);
            holder.cfu.setText(mData[position][1]);

            Context context = mContext.getApplicationContext();
            final DbManager db = new DbManager(mContext);

            docente = db.leggiDocente(mData[position][2]);
            holder.professor_name.setText(docente[1]);
            holder.professor_surname.setText(docente[2]);

    }

    @Override
    public int getItemCount() {

        return mData.length;
    }

    public class examviewholder extends RecyclerView.ViewHolder{
        private RelativeLayout professorIcon;
        TextView title, cfu, professor_name, professor_surname;
        public examviewholder(@NonNull View itemView) {
            super(itemView);
            professorIcon = itemView.findViewById(R.id.professor);
            title = itemView.findViewById(R.id.item_exam_title);
            professor_name= itemView.findViewById(R.id.item_name_professor);
            professor_surname = itemView.findViewById(R.id.item_surname_professor);
            cfu = itemView.findViewById(R.id.item_cfu);
        }
    }
}
