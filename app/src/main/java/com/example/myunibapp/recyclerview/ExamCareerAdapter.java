package com.example.myunibapp.recyclerview;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myunibapp.DbManager;
import com.example.myunibapp.R;


public class ExamCareerAdapter extends RecyclerView.Adapter<ExamCareerAdapter.examviewholder> {
    private static final String TAG_LOG = ExamCareerAdapter.class.getName();
    public static final short CAMPI_DB_DOCENTE = 5;
    Context mContext;
    String[][] mData;
    Dialog myDialog;
    public String[] docente = new String[CAMPI_DB_DOCENTE];
    int ciao;
    int pollo;


    public ExamCareerAdapter(Context mContext, String[][] mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public examviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_exam_career, parent, false);
        final examviewholder vHolder = new examviewholder(view);


        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull examviewholder holder, int position) {
            holder.title.setText(mData[position][0]);
            holder.voto.setText(mData[position][1]);
            holder.data.setText(mData[position][2]);
            holder.cfu.setText(mData[position][3]);

            Context context = mContext.getApplicationContext();
            final DbManager db = new DbManager(mContext);


    }

    @Override
    public int getItemCount() {

        return mData.length;
    }

    public class examviewholder extends RecyclerView.ViewHolder{

        TextView title, cfu, voto, data;
        public examviewholder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.item_exam_title);
            voto = itemView.findViewById(R.id.voto_numerico);
            cfu = itemView.findViewById(R.id.item_cfu);
            data = itemView.findViewById(R.id.data);
        }
    }
}
