package com.example.myunibapp.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myunibapp.DbManager;
import com.example.myunibapp.MainActivity;
import com.example.myunibapp.R;
import com.example.myunibapp.ui.home.HomeFragment;
import com.example.myunibapp.ui.home.HomeViewModel;


public class ProfileFragment extends Fragment {
    public static final short CAMPI_DB_STUDENTE = 9;
    private static final String TAG_LOG = HomeFragment.class.getName();
    private HomeViewModel homeViewModel;

    public String[] studente = new String[CAMPI_DB_STUDENTE];

    TextView t1;
    String strtext;
    String value1;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);

        MainActivity activity = (MainActivity) getActivity();
        Bundle results = activity.getMyData();
        value1 = results.getString("val1");
        Log.d(TAG_LOG, "Ecco cosa sta nel fragment ciaooo " + value1);

        Context context = root.getContext();
        final DbManager db = new DbManager(context);

        studente = db.leggiStudente(value1);

        TextView matricola = root.findViewById(R.id.matricola);
        matricola.setText(studente[0]);

        TextView nome = root.findViewById(R.id.name);
        nome.setText(studente[1]);

        TextView cognome = root.findViewById(R.id.surname);
        cognome.setText(studente[2]);

        TextView email = root.findViewById(R.id.email);
        email.setText(studente[3]);

        TextView corso = root.findViewById(R.id.corso);
        corso.setText(studente[8]);

        Window window = activity.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(activity,R.color.background_darker));

        return root;
    }
}
