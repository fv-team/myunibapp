package com.example.myunibapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBhelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "unibapp";
    private static final short DB_VERSION = 1;
    private static final String QUERY_CREATION_SCHEMA_STUDENTI =

            "CREATE TABLE IF NOT EXISTS Studenti("
            + "Matricola INT PRIMARY KEY,"
            + "NomeStudente VARCHAR,"
            + "CognomeStudente VARCHAR,"
            + "EmailStudente VARCHAR,"
            + "PasswordStudente VARCHAR,"
            + "MediaStudente DECIMAL,"
            + "PercEsamiSuperati DECIMAL,"
            + "CreditiAcquisiti INT,"
            + "CorsoDiLaurea VARCHAR"
            + ");";

     private static final String QUERY_CREATION_SCHEMA_DOCENTI =

            "CREATE TABLE IF NOT EXISTS Docenti("
            + "IdDocente INT PRIMARY KEY,"
            + "NomeDocente VARCHAR,"
            + "CognomeDocente VARCHAR,"
            + "EmailDocente VARCHAR,"
            + "RicevimentoDocente VARCHAR"
            + ");";

     private static final String QUERY_CREATION_SCHEMA_ESAMI =

            "CREATE TABLE IF NOT EXISTS Esami("
            + "IdEsame INT PRIMARY KEY,"
            + "NomeEsame VARCHAR,"
            + "PesoInCrediti INT,"
            + "AnnoEsame INT,"
            + "IdDocente INT,"
            + "FOREIGN KEY (IdDocente) REFERENCES Docenti(IdDocente)"
            + "ON DELETE SET NULL "
            + "ON UPDATE CASCADE"
            + ");";

     private static final String QUERY_CREATION_SCHEMA_ESAMISUPERATI =

             "CREATE TABLE IF NOT EXISTS EsamiSuperati("
             + "Id INT PRIMARY KEY,"
             + "IdEsame INT,"
             + "Matricola INT,"
             + "VotoEsame INT DEFAULT 0,"
             + "DataEsame DATE DEFAULT '0001/01/01',"
             + "FOREIGN KEY (IdEsame) REFERENCES Esami(IdEsame)"
             + "ON DELETE SET NULL "
             + "ON UPDATE CASCADE,"
             + "FOREIGN KEY (Matricola) REFERENCES Studenti(Matricola)"
             + "ON DELETE SET NULL "
             + "ON UPDATE CASCADE"
             + ");";

    public DBhelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //Metodo che crea lo schema del db. Il db viene già creato automaticamente da Android
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(QUERY_CREATION_SCHEMA_STUDENTI);
        db.execSQL(QUERY_CREATION_SCHEMA_DOCENTI);
        db.execSQL(QUERY_CREATION_SCHEMA_ESAMI);
        db.execSQL(QUERY_CREATION_SCHEMA_ESAMISUPERATI);
    }

    //Metodo che permette di aggiornare il db su disco  alla versione richiesta
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS Studenti");
        db.execSQL("DROP TABLE IF EXISTS Docenti");
        db.execSQL("DROP TABLE IF EXISTS Esami");
        db.execSQL("DROP TABLE IF EXISTS EsamiSuperati");
        onCreate(db);
    }
}
