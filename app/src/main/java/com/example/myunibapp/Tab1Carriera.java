package com.example.myunibapp;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myunibapp.recyclerview.ExamAdapter;
import com.example.myunibapp.recyclerview.ExamCareerAdapter;

public class Tab1Carriera extends Fragment  {
    private static final String TAG_LOG = Tab1Carriera.class.getName();
    private RecyclerView rvExams;
    private ExamCareerAdapter examCareerAdapter;
    View v;
    private String[][] piano_di_studi;
    String value1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_tab1_carriera, container, false);
        initViews();
        setupExamAdapter();


        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Context context = getActivity().getApplicationContext();
        final DbManager db = new DbManager(context);
        super.onCreate(savedInstanceState);
        piano_di_studi = db.leggiPianoDiStudi("1");
    }

    private void setupExamAdapter() {
        Carriera activity = (Carriera) getActivity();
        Bundle results = activity.getMyData();
        value1 = results.getString("val1");
        Context context = getActivity().getApplicationContext();
        final DbManager db = new DbManager(context);
        String[][] esamiSuperati = new String[db.contaEsamiSuperati(value1)][4];
        esamiSuperati = db.leggiEsamiSuperati(value1);
        examCareerAdapter = new ExamCareerAdapter(getContext(), esamiSuperati);
        rvExams.setAdapter(examCareerAdapter);
        rvExams.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initViews() {
        rvExams = (RecyclerView) v.findViewById(R.id.rv_exam_career);
        rvExams.setLayoutManager(new LinearLayoutManager(getActivity()));
        //rvExamsCareer.setHasFixedSize(true);
    }

}
