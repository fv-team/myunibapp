package com.example.myunibapp;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myunibapp.recyclerview.ExamCareerAdapter;
import com.example.myunibapp.recyclerview.ExamCareerAdapterNonSuperati;

public class Tab2Carriera extends Fragment  {
    private static final String TAG_LOG = Tab2Carriera.class.getName();
    private RecyclerView rvExams;
    private ExamCareerAdapterNonSuperati examCareerAdapter;
    View v;
    private String[][] piano_di_studi;
    TextView messaggio;
    String value1;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_tab2_carriera, container, false);
        initViews();
        setupExamAdapter();


        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Context context = getActivity().getApplicationContext();
        final DbManager db = new DbManager(context);
        super.onCreate(savedInstanceState);
        piano_di_studi = db.leggiPianoDiStudi("1");


    }

    private void setupExamAdapter() {
        Carriera activity = (Carriera) getActivity();
        Bundle results = activity.getMyData();
        value1 = results.getString("val1");
        Context context = getActivity().getApplicationContext();
        final DbManager db = new DbManager(context);
        String[][] esamiNonSuperati = new String[db.contaEsamiNonSuperati(value1)][4];
        esamiNonSuperati = db.leggiEsamiNonSuperati(value1);

        //Stampa un messaggio se si sono superati tutti gli esami
        //E' un pretesto anche per utilizzare i plurals
        if(db.contaEsamiNonSuperati(value1) != 0) {
            examCareerAdapter = new ExamCareerAdapterNonSuperati(getContext(), esamiNonSuperati);
            rvExams.setAdapter(examCareerAdapter);
            rvExams.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
        else {
            messaggio = (TextView) v.findViewById(R.id.messaggio_esami_superati);
            messaggio.setText(R.string.msg_finiti_esami);
        }
    }



    private void initViews() {
        rvExams = (RecyclerView) v.findViewById(R.id.rv_exam_career);
        rvExams.setLayoutManager(new LinearLayoutManager(getActivity()));
        //rvExamsCareer.setHasFixedSize(true);
    }

}
