package com.example.myunibapp;

import com.example.myunibapp.Model.MyPlaces;
import com.example.myunibapp.Model.Results;
import com.example.myunibapp.Remote.IGoogleAPIService;
import com.example.myunibapp.Remote.RetrofitClient;

public class Common {

    public static Results currentResult;

    private static final String GOOGLE_API_URL = "https://maps.googleapis.com/";

    public static IGoogleAPIService getGoogleApiService()
    {
        return RetrofitClient.getClient(GOOGLE_API_URL).create(IGoogleAPIService.class);

    }
}
