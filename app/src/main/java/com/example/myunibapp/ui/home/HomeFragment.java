package com.example.myunibapp.ui.home;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.myunibapp.Carriera;
import com.example.myunibapp.DbManager;
import com.example.myunibapp.EsploraActivity;
import com.example.myunibapp.MainActivity;
import com.example.myunibapp.R;
import com.example.myunibapp.StatisticheActivity;
import com.example.myunibapp.PianoDiStudi;

public class HomeFragment extends Fragment implements View.OnClickListener {

    public static final short CAMPI_DB_STUDENTE = 9;
    private static final String TAG_LOG = HomeFragment.class.getName();
    private LinearLayout carriera, esplora, statistiche, pianodistudi;
    ImageView logo;
    private HomeViewModel homeViewModel;

    public String[] studente = new String[CAMPI_DB_STUDENTE];

    TextView t1;
    String strtext;
    String value1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        homeViewModel =
              new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        MainActivity activity = (MainActivity) getActivity();
        Bundle results = activity.getMyData();
        value1 = results.getString("val1");
        Log.d(TAG_LOG, "Ecco cosa sta nel fragment ciaooo " + value1);

        Context context = root.getContext();
        final DbManager db = new DbManager(context);

        studente = db.leggiStudente(value1);

        TextView nome = (TextView) root.findViewById(R.id.name);
        nome.setText(studente[1]);

        logo  = (ImageView) root.findViewById(R.id.imageView7);

        logo.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.uniba.it/"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });


        //defining Cards
        carriera =  root.findViewById(R.id.carriera);
        esplora = root.findViewById(R.id.esplora);
        statistiche = root.findViewById(R.id.statistiche);
        pianodistudi = root.findViewById(R.id.pianodistudi);


        //add Click listener to the cards
        carriera.setOnClickListener(this);
        esplora.setOnClickListener(this);
        statistiche.setOnClickListener(this);
        pianodistudi.setOnClickListener(this);


        Window window = activity.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(activity,R.color.background));

        return root;

    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onClick(View root) {
        Intent i;
        Bundle data = new Bundle();

        switch (root.getId()) {
            case R.id.carriera:
                i = new Intent(root.getContext(), Carriera.class);
                data.putString("data",value1);
                i.putExtras(data);
                Log.d(TAG_LOG, "Valore che passo alla carriera " + data);
                startActivity(i);
                break;
            case R.id.esplora:
                i = new Intent(root.getContext(), EsploraActivity.class);
                startActivity(i);
                break;
            case R.id.statistiche:
                i = new Intent(root.getContext(), StatisticheActivity.class);
                data.putString("data",value1);
                i.putExtras(data);
                Log.d(TAG_LOG, "Valore che passo alla statistica " + data);
                startActivity(i);
                break;
            case R.id.pianodistudi:
                i = new Intent(root.getContext(), PianoDiStudi.class);
                startActivity(i);
                break;
            default:break;
        }
    }

}
