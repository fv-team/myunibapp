package com.example.myunibapp.ui.login;

import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myunibapp.MainActivity;
import com.example.myunibapp.R;

import com.example.myunibapp.DbManager;

import java.util.Arrays;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG_LOG = LoginActivity.class.getName();
    private LoginViewModel loginViewModel;

    public static final short CAMPI_DB_STUDENTE = 9;
    public static final short CAMPI_DB_DOCENTE = 5;
    public static final short CAMPI_DB_ESAME = 4;
    public static final short TOT_ESAMI = 21;

    public String[] studente = new String[CAMPI_DB_STUDENTE]; //Dove 9 è il numero dei campi nel db della tabella studenti
    public String[] docente = new String[CAMPI_DB_DOCENTE]; //Dove 4 è il numero dei campi nel db della tabella docenti
    public String[] esame = new String[CAMPI_DB_ESAME]; //Dove 4 è il numero dei campi nel db della tabella esami
    public String[][] pianoDiStudi = new String[TOT_ESAMI][3]; //Dove 21 è il numero degli esami nel piano di studi di ITPS e 3 NomeEsame , cfu , IdDocente

    float mediaStudente1 = 0; //Media aritmetica dello studente
    float mediaPonderataStudente1 = 0; //Media ponderata dello studente
    float percentualeEsamiSuperati1 = 0; //Percentuale degli esami superati
    int creditiAcquisiti1 = 0; //Crediti acquisiti dagli esami superati
    int votoLaurea1 = 0; //Voto di Laurea

    float mediaStudente2 = 0; //Media aritmetica dello studente
    float mediaPonderataStudente2 = 0; //Media ponderata dello studente
    float percentualeEsamiSuperati2 = 0; //Percentuale degli esami superati
    int creditiAcquisiti2 = 0; //Crediti acquisiti dagli esami superati
    int votoLaurea2 = 0; //Voto di Laurea

    public String matricolaDigitata = "0";
    public String passwordDigitata = "0";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        Context context = getApplicationContext();
        final DbManager db = new DbManager(context); //Prima del login dinamico non era final

        //NB: SE CI DOVESSERO ESSERE PROBLEMI SUI VALORI FORSE E' PERCHE' NEL DB IL VOTO E' INT E NOI LO GESTIAMO COME STRING

        //Inseriamo i docenti nel db
        //SFRUTTARE SIA PER I DOCENTI CHE PER GLI ESAMI IL RETURN DI INSERT PER LA VERIFICA

        short[] esitoInserimentoDocente = new short[TOT_ESAMI];

        esitoInserimentoDocente[0] = db.inserisciDocente("1", "Giovanni", "Dimauro", "giovanni.dimauro@uniba.it" , "Lunedì dalle 9:00 alle 11:00 Piano 6 Stanza 114 Informatica");
        esitoInserimentoDocente[1] = db.inserisciDocente("2", "Giovanna", "Castellano", "giovanna.castellano@uniba.it" , "Martedì dalle 14:00 alle 16:00 Piano 6 Stanza 101 Dipartimento di Informatica");
        esitoInserimentoDocente[2] = db.inserisciDocente("3", "Vincenzo", "Nardozza", "vincenzo.nardozza@uniba.it" , "Lunedì dalle 15:00 alle 18:00 Piano 3 Stanza 15 Dipartimento di Matematica");
        esitoInserimentoDocente[3] = db.inserisciDocente("4", "Lorenzo", "D'Ambrosio", "lorenzo.dambrosio@uniba.it" , "Su appuntamento Piano 3 Stanza 13 Dipartimento di Matematica");
        esitoInserimentoDocente[4] = db.inserisciDocente("5", "Emanuele", "Covino", "emanuele.covino@uniba.it" ,  "Martedì dalle 11:00 alle 12:00 Piano 6 Stanza 115 Dipartimento di Informatica");
        esitoInserimentoDocente[5] = db.inserisciDocente("6", "Cataldo", "Musto", "cataldo.musto@uniba.it" ,  "Giovedì dalle 15.00 alle 17.00 Piano 7 Stanza 762 Dipartimento di Informatica");
        esitoInserimentoDocente[6] = db.inserisciDocente("7", "Antonietta", "Bagnardi", "antonietta.bagnardi@uniba.it" ,  "Martedì dalle 12:00 alle 13:30/Giovedì dalle 12:00 alle 13:30 Piano 6 Stanza 115 Dipartimento di Informatica");
        esitoInserimentoDocente[7] = db.inserisciDocente("8", "Nicola", "Boffoli", "nicola.fanizzi@uniba.it" ,  "Martedì dalle 15:30 alle 17:30 o per appuntamento Piano 5 Stanza 22 Dipartimento di Informatica");
        esitoInserimentoDocente[8] = db.inserisciDocente("9", "Francesca", "Lisi", "francesca.lisi@uniba.it" ,  "Su appuntamento Piano 5 Stanza 515 Dipartimento di Informatica");
        esitoInserimentoDocente[9] = db.inserisciDocente("10", "Roberto", "Garrappa", "roberto.garrappa@uniba.it" ,  " Giovedì dalle 15.00 alle 16.30 o per appuntamento Piano 3 Stanza 7 Dipartimento di Matematica");
        esitoInserimentoDocente[10] = db.inserisciDocente("11", "Nicole", "Novielli", "nicole.novielli@uniba.it" ,  "Venerdì dalle 12:00 alle 14:00 Piano 6 Stanza 119 Dipartimento di Informatica");
        esitoInserimentoDocente[11] = db.inserisciDocente("12", "Antonio", "Piccinno", "antonio.piccinno@uniba.it" ,  "Mercoledì dalle 10:30 alle 11:30 Piano 6 Stanza 619 Dipartimento di Informatica");
        esitoInserimentoDocente[12] = db.inserisciDocente("13", "Daniela", "Cozza", "daniela.cozza@uniba.it" ,  "Su appuntamento Piano 3 Stanza 10 Dipartimento di Fisica");
        esitoInserimentoDocente[13] = db.inserisciDocente("14", "Vitonofrio", "Crismale", "vitonofrio.crismale@uniba.it" ,  "Venerdì dalle 15:00 alle 17:00 Piano 2 Stanza 12 Dipartimento di Matematica");
        esitoInserimentoDocente[14] = db.inserisciDocente("15", "Gaetano", "Macario", "gaetano.macario@uniba.it" ,  "Giovedì dalle 09:00 alle 11:00 Piano 2 Stanza 02 Dipartimento di Economia e Finanza");
        esitoInserimentoDocente[15] = db.inserisciDocente("16", "Maria Teresa", "Baldassarre", "maria.baldassarre@uniba.it" ,  "Giovedì dalle 11.00 alle 13.00 Piano 6 Stanza 121 Dipartimento di Informatica");
        esitoInserimentoDocente[16] = db.inserisciDocente("17", "Michele", "Scalera", "michele.scalera@uniba.it" ,  "Lunedì dalle 14:30 alle 16:30 Piano 6 Stanza 619 Dipartimento di Informatica");
        esitoInserimentoDocente[17] = db.inserisciDocente("18", "Berardina Nadja", "De Carolis", "berardina.decarolis@uniba.it" ,  "Venerdì dalle 10:30 alle 12:30 Piano 6 Stanza 109 Dipartimento di Informatica");
        esitoInserimentoDocente[18] = db.inserisciDocente("19", "Paolo", "Buono", "paolo.buono@uniba.it" ,  "Giovedì dalle 11:00 alle 13:00 Piano 6 Stanza 517 Dipartimento di Informatica");
        esitoInserimentoDocente[19] = db.inserisciDocente("20", "Enrichetta", "Gentile", "enrichetta.gentile@uniba.it" , "Venerdi' dalle 10.30 alle 12.30 Piano 6 Stanza 145 Dipartimento di Informatica");
        esitoInserimentoDocente[20] = db.inserisciDocente("21", "Donato", "Impedovo", "donato.impedovo@uniba.it" ,  "Mercoledì dalle 11:30 alle 13:00 Piano 6 Stanza 610 Dipartimento di Informatica");

        //Inseriamo gli esami del piano di studi nel db
        short[] esitoInserimentoEsame = new short[TOT_ESAMI];

        esitoInserimentoEsame[0] = db.inserisciEsame("1" , "Programmazione", "12" , "1" , "1");
        esitoInserimentoEsame[1] = db.inserisciEsame("2" , "Architettura degli Elaboratori e Sistemi Operativi"  ,"9", "1",  "2");
        esitoInserimentoEsame[2] = db.inserisciEsame("3" , "Matematica Discreta"  , "9" , "1" ,  "3");
        esitoInserimentoEsame[3] = db.inserisciEsame("4" , "Analisi Matematica" , "9" , "1" , "4");
        esitoInserimentoEsame[4] = db.inserisciEsame("5" , "Linguaggi di Programmazione" , "9" , "1" ,  "5");
        esitoInserimentoEsame[5] = db.inserisciEsame("6" , "Laboratorio di Informatica" , "6" , "1" ,  "6");
        esitoInserimentoEsame[6] = db.inserisciEsame("7" , "Lingua Inglese" , "6" , "1" , "7");
        esitoInserimentoEsame[7] = db.inserisciEsame("8" , "Programmazione II" , "9" , "2" , "8");
        esitoInserimentoEsame[8] = db.inserisciEsame("9" , "Progettazione di Basi di Dati" , "9", "2" , "9");
        esitoInserimentoEsame[9] = db.inserisciEsame("10" , "Calcolo Numerico" , "6", "2" , "10");
        esitoInserimentoEsame[10] = db.inserisciEsame("11" , "Reti di Calcolatori" , "6" , "2" ,  "11");
        esitoInserimentoEsame[11] = db.inserisciEsame("12" , "Ingegneria del Software" , "12", "2" , "12");
        esitoInserimentoEsame[12] = db.inserisciEsame("13" , "Fisica Applicata all'Informatica" , "6", "2" , "13");
        esitoInserimentoEsame[13] = db.inserisciEsame("14" , "Statistica per l'Ingengeria del Software" , "6", "2" , "14");
        esitoInserimentoEsame[14] = db.inserisciEsame("15" , "Economia e Gestione d'Impresa" , "6" , "2" ,  "15");
        esitoInserimentoEsame[15] = db.inserisciEsame("16" , "Modelli e Metodi per la Qualità del Software" , "9" , "3" , "16");
        esitoInserimentoEsame[16] = db.inserisciEsame("17" , "Integrazione e Test di Sistemi Software" , "6" , "3" , "17");
        esitoInserimentoEsame[17] = db.inserisciEsame("18" , "Progettazione dell'Interazione Utente + Lab" , "6" , "3" ,  "18");
        esitoInserimentoEsame[18] = db.inserisciEsame("19" , "Sviluppo di Mobile Software" ,"9" , "3" , "19");
        esitoInserimentoEsame[19] = db.inserisciEsame("20" , "Sistemi Informativi sul Web" , "6" , "3" ,  "20");
        esitoInserimentoEsame[20] = db.inserisciEsame("21" , "Sicurezza Applicazioni" , "6" , "3" , "21");

        //Inseriamo gli esami superati nel db
        //TODO: da aumentare la popolazione
        short[] esitoInserimentoEsameSuperato = new short[TOT_ESAMI*2]; //21 Esami totali * 2 utenti totali = 42 esami totali che potrebbero essere superati
        //ho messo 36 esami superati totali di cui 21 dal primo studente (quindi tutti) e 15 dal secondo studente che quindi avrà degli esami ancora da fare

        esitoInserimentoEsameSuperato[0] = db.inserisciEsameSuperato("1" , "1" , "1" , "29" , "2018/01/15");
        esitoInserimentoEsameSuperato[1] = db.inserisciEsameSuperato("2" , "2" , "1" , "30" , "2018/01/30");
        esitoInserimentoEsameSuperato[2] = db.inserisciEsameSuperato("3" , "3" , "1" , "19" , "2018/02/2");
        esitoInserimentoEsameSuperato[3] = db.inserisciEsameSuperato("4" , "4" , "1" , "18" , "2018/06/30");
        esitoInserimentoEsameSuperato[4] = db.inserisciEsameSuperato("5" , "5" , "1" , "23" , "2018/07/02");
        esitoInserimentoEsameSuperato[5] = db.inserisciEsameSuperato("6" , "6" , "1" , "26" , "2018/07/25");
        esitoInserimentoEsameSuperato[6] = db.inserisciEsameSuperato("7" , "7" , "1" , "26" , "2018/07/30");
        esitoInserimentoEsameSuperato[7] = db.inserisciEsameSuperato("8" , "8" , "1" , "28" , "2018/09/25");
        esitoInserimentoEsameSuperato[8] = db.inserisciEsameSuperato("9" , "9" , "1" , "27" , "2019/01/20");
        esitoInserimentoEsameSuperato[9] = db.inserisciEsameSuperato("10" , "10" , "1" , "23" , "2019/01/28");
        esitoInserimentoEsameSuperato[10] = db.inserisciEsameSuperato("11" , "11" , "1" , "30" , "2019/02/14");
        esitoInserimentoEsameSuperato[11] = db.inserisciEsameSuperato("12" , "12" , "1" , "27" , "2019/04/28");
        esitoInserimentoEsameSuperato[12] = db.inserisciEsameSuperato("13" , "13" , "1" , "24" , "2019/06/15");
        esitoInserimentoEsameSuperato[13] = db.inserisciEsameSuperato("14" , "14" , "1" , "24" , "2019/06/25");
        esitoInserimentoEsameSuperato[14] = db.inserisciEsameSuperato("15" , "15" , "1" , "24" , "2019/07/10");
        esitoInserimentoEsameSuperato[15] = db.inserisciEsameSuperato("16" , "16" , "1" , "26" , "2019/07/28");
        esitoInserimentoEsameSuperato[16] = db.inserisciEsameSuperato("17" , "17" , "1" , "27" , "2019/09/20");
        esitoInserimentoEsameSuperato[17] = db.inserisciEsameSuperato("18" , "18" , "1" , "30" , "2020/01/30");
        esitoInserimentoEsameSuperato[18] = db.inserisciEsameSuperato("19" , "19" , "1" , "30" , "2020/06/30");
        esitoInserimentoEsameSuperato[19] = db.inserisciEsameSuperato("20" , "20" , "1" , "28" , "2020/04/10");
        esitoInserimentoEsameSuperato[20] = db.inserisciEsameSuperato("21" , "21" , "1" , "30" , "2020/06/30");


        esitoInserimentoEsameSuperato[21] = db.inserisciEsameSuperato("22" , "1" , "2" , "30" , "2018/01/15");
        esitoInserimentoEsameSuperato[22] = db.inserisciEsameSuperato("23" , "2" , "2" , "30" , "2018/01/30");
        esitoInserimentoEsameSuperato[23] = db.inserisciEsameSuperato("24" , "3" , "2" , "24" , "2018/02/2");
        esitoInserimentoEsameSuperato[24] = db.inserisciEsameSuperato("25" , "4" , "2" , "26" , "2018/06/30");
        esitoInserimentoEsameSuperato[25] = db.inserisciEsameSuperato("26" , "5" , "2" , "28" , "2018/07/02");
        esitoInserimentoEsameSuperato[26] = db.inserisciEsameSuperato("27" , "6" , "2" , "27" , "2018/07/25");
        esitoInserimentoEsameSuperato[27] = db.inserisciEsameSuperato("28" , "7" , "2" , "30" , "2018/07/30");
        esitoInserimentoEsameSuperato[28] = db.inserisciEsameSuperato("29" , "8" , "2" , "30" , "2018/09/25");
        esitoInserimentoEsameSuperato[29] = db.inserisciEsameSuperato("30" , "9" , "2" , "18" , "2019/01/20");
        esitoInserimentoEsameSuperato[30] = db.inserisciEsameSuperato("31" , "10" , "2" , "27" , "2019/01/28");
        esitoInserimentoEsameSuperato[31] = db.inserisciEsameSuperato("32" , "11" , "2" , "30" , "2019/02/14");
        esitoInserimentoEsameSuperato[32] = db.inserisciEsameSuperato("33" , "12" , "2" , "25" , "2019/04/28");
        esitoInserimentoEsameSuperato[33] = db.inserisciEsameSuperato("34" , "15" , "2" , "24" , "2019/07/10");
        esitoInserimentoEsameSuperato[34] = db.inserisciEsameSuperato("35" , "18" , "2" , "30" , "2020/01/30");
        esitoInserimentoEsameSuperato[35] = db.inserisciEsameSuperato("36" , "21" , "2" , "24" , "2020/06/30");


        //Inseriamo gli studenti nel db dopo aver calcolato le statistiche
        mediaStudente1 = db.calcolaMediaStudente("1");
        //Toast.makeText(getApplicationContext(), Float.toString(mediaStudente1), Toast.LENGTH_LONG).show();

        mediaPonderataStudente1 = db.calcolaMediaPonderataStudente("1");
        //Toast.makeText(getApplicationContext(), Float.toString(mediaPonderataStudente1), Toast.LENGTH_LONG).show();

        percentualeEsamiSuperati1 = db.calcoloPercEsamiSuperati("1");
        //Toast.makeText(getApplicationContext(), Float.toString(percentualeEsamiSuperati1), Toast.LENGTH_LONG).show();

        creditiAcquisiti1 = db.calcoloCreditiAcquisiti("1");
        //Toast.makeText(getApplicationContext(), Integer.toString(creditiAcquisiti1) , Toast.LENGTH_LONG).show();

        votoLaurea1 = db.calcolaVotoLaurea("1" , mediaPonderataStudente1);
        //Toast.makeText(getApplicationContext(), Integer.toString(votoLaurea1), Toast.LENGTH_LONG).show();


        mediaStudente2 = db.calcolaMediaStudente("2");
        Log.d(TAG_LOG, "Ecco la media dello studente2 " + mediaStudente2);
        Log.d(TAG_LOG, "Ecco la media  " + Arrays.toString(pianoDiStudi));
        //Toast.makeText(getApplicationContext(), Float.toString(mediaStudente2), Toast.LENGTH_LONG).show();

        mediaPonderataStudente2 = db.calcolaMediaPonderataStudente("2");
        //Toast.makeText(getApplicationContext(), Float.toString(mediaPonderataStudente1), Toast.LENGTH_LONG).show();

        percentualeEsamiSuperati2 = db.calcoloPercEsamiSuperati("2");
        //Toast.makeText(getApplicationContext(), Float.toString(percentualeEsamiSuperati2), Toast.LENGTH_LONG).show();

        creditiAcquisiti2 = db.calcoloCreditiAcquisiti("2");
        //Toast.makeText(getApplicationContext(), Integer.toString(creditiAcquisiti2) , Toast.LENGTH_LONG).show();

        votoLaurea2 = db.calcolaVotoLaurea("2" , mediaPonderataStudente2);
        //Toast.makeText(getApplicationContext(), Integer.toString(votoLaurea2), Toast.LENGTH_LONG).show();


        long esitoStudente1 = db.inserisciStudente(1 , "Mario" , "Rossi" , "mario@rossi.it" , "password" , mediaStudente1 , percentualeEsamiSuperati1 , creditiAcquisiti1 , "Laurea Triennale in Informatica e Tecnologie per la Produzione del Software (D.M. 270) ");
        long esitoStudente2 = db.inserisciStudente(2 , "Luigi" , "Verdi" , "luigi@verdi.it" , "password" , mediaStudente2 , percentualeEsamiSuperati2 , creditiAcquisiti2 , "Laurea Triennale in Informatica e Tecnologie per la Produzione del Software (D.M. 270) ");


        studente = db.leggiStudente("1");

        Log.d(TAG_LOG, "Ecco studenteeee " + Arrays.toString(studente));
        //Toast.makeText(getApplicationContext(), studente[7] + " " + studente[1] + " " + studente[2], Toast.LENGTH_LONG).show();

        //Toast.makeText(getApplicationContext(), studente[8] + " " + studente[1] + " " + studente[2], Toast.LENGTH_LONG).show();


        docente = db.leggiDocente("11");
        //Toast.makeText(getApplicationContext(), docente[0] + " " + docente[1] + " " + docente[4], Toast.LENGTH_LONG).show();

        //esame = db.leggiEsame("21");
        //Toast.makeText(getApplicationContext(), esame[3] + " " + esame[1] + " " + esame[2], Toast.LENGTH_LONG).show();


       // pianoDiStudi = db.leggiPianoDiStudi("3");
        //Log.d(TAG_LOG, "Piano di studi: " + pianoDiStudi[12][0]);
        //Toast.makeText(getApplicationContext(), pianoDiStudi[0] + " " + pianoDiStudi[1] + " " + pianoDiStudi[2], Toast.LENGTH_LONG).show();

        //pianoDiStudi = db.leggiPianoDiStudi("3");
        //Toast.makeText(getApplicationContext(), pianoDiStudi[0][0] + " " + pianoDiStudi[0][1] + " " + pianoDiStudi[0][2], Toast.LENGTH_LONG).show();

        //String[][] esamiSuperati = new String[db.contaEsamiSuperati("2")][4];
        //esamiSuperati = db.leggiEsamiSuperati("2");
        //Toast.makeText(getApplicationContext(), esamiSuperati[0][0] + " " + esamiSuperati[0][1] + " " + esamiSuperati[0][2] + " " + esamiSuperati[0][3], Toast.LENGTH_LONG).show();

        //String[][] esamiNonSuperati = new String[db.contaEsamiNonSuperati("2")][3];
        //esamiNonSuperati = db.leggiEsamiNonSuperati("2");
        //Toast.makeText(getApplicationContext(), esamiNonSuperati[0][0] + " " + esamiNonSuperati[0][1] + " " + esamiNonSuperati[0][2], Toast.LENGTH_LONG).show();
        //Toast.makeText(getApplicationContext(), Short.toString(db.contaEsamiNonSuperati("2")), Toast.LENGTH_LONG).show();


        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResult.getError() != null) {
                    showLoginFailed(loginResult.getError());
                }
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                }
                setResult(Activity.RESULT_OK);

                //Complete and destroy login activity once successful
                finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                matricolaDigitata = usernameEditText.getText().toString();
                passwordDigitata = passwordEditText.getText().toString();


                studente = db.leggiStudente(matricolaDigitata);

                loadingProgressBar.setVisibility(View.VISIBLE);
                loginViewModel.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        });
    }

    private void updateUiWithUser(LoggedInUserView model) {
        //Se studente[0] che è la matricola dello studente letta dalla matricola digitata è uguale alla matricola digitata
        //vuol dire che lo studente esiste nel db, altrimenti no

        if(studente[0].equals(matricolaDigitata)  && studente[4].equals(passwordDigitata)) {
        //String welcome = getString(R.string.welcome) + model.getDisplayName();
            //String welcome = "Benvenuto! " + " " + studente[1] + " " + studente[2];
            // TODO : initiate successful logged in experience
            //Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
            // We create the explicit Intent
            final Intent intent = new Intent(this, MainActivity.class);
            Bundle data = new Bundle();
            data.putString("data",matricolaDigitata);
            intent.putExtras(data);
            Log.d(TAG_LOG, "Ecco cosa sta " + data);
            // Launch the Intent
            startActivity(intent);
        }
        else {
            Toast.makeText(getApplicationContext(), R.string.login_error, Toast.LENGTH_LONG).show();
            final Intent intent = new Intent(this, LoginActivity.class);
            // Launch the Intent
            startActivity(intent);
        }
        }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}
