package com.example.myunibapp;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PagerAdapterCareer extends FragmentStatePagerAdapter {
    private static final String TAG_LOG = PagerAdapterCareer.class.getName();
    int countTabs;

    public PagerAdapterCareer(@NonNull FragmentManager fm, int countTabs) {
        super(fm);
        this.countTabs = countTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                Tab1Carriera tab1 = new Tab1Carriera();
                return tab1;
            case 1:
                Tab2Carriera tab2 = new Tab2Carriera();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return countTabs;
    }



}
